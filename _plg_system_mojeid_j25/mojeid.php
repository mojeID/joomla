<?php
/**
* Created on Mar 9, 2013
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

class plgSystemMojeID extends JPlugin {
	function __construct($config,$params) {
		parent::__construct($config,$params);
		$this->loadLanguage();
	}
	
	function onAfterInitialise() {
		if($_SERVER["HTTP_ACCEPT"]=='application/xrds+xml') {
			ob_start();
			?>
<xrds:XRDS xmlns:xrds="xri://$xrds" xmlns="xri://$xrd*($v*2.0)">
	<XRD>
		<Service>
			<Type>http://specs.openid.net/auth/2.0/return_to</Type>
			<URI><?php echo JFactory::getURI()->base(); ?></URI>
		</Service>
		<Service>
			<Type>http://specs.nic.cz/registration/assert_url</Type>
			<URI><?php echo str_replace('http://','https://',JFactory::getURI()->base()); ?>index.php?mojeid_login=4</URI>
		</Service>
	</XRD>
</xrds:XRDS>
			<?php
			$xrds=ob_get_contents();
			ob_end_clean();
			
			header("Content-type: text/xml");
			echo $xrds;
			JFactory::getApplication()->close();
		}
		if(JFactory::getDocument()->getType()=='html') {
			JFactory::getDocument()->addCustomTag('<meta http-equiv="x-xrds-location" content="https://'.JFactory::getURI()->toString(array('host','path')).'index.php?mojeid_login=3" />');
		}
	}
	
	function onAfterRoute() {
		switch(JRequest::getInt('mojeid_login',0)) {
			case 1:
				echo $this->_getLogin();
				JFactory::getApplication()->close();
				break;
			case 2:
				jimport('joomla.user.authentication');
				JFactory::getApplication()->login(array(),array());
				JFactory::getApplication()->redirect('index.php');
				break;
			case 3:
				ob_start();
				?>
	<xrds:XRDS xmlns:xrds="xri://$xrds" xmlns="xri://$xrd*($v*2.0)">
		<XRD>
			<Service>
				<Type>http://specs.openid.net/auth/2.0/return_to</Type>
				<URI><?php echo JFactory::getURI()->base(); ?></URI>
			</Service>
			<Service>
				<Type>http://specs.nic.cz/registration/assert_url</Type>
				<URI><?php echo str_replace('http://','https://',JFactory::getURI()->base()); ?>index.php?mojeid_login=4</URI>
			</Service>
		</XRD>
	</xrds:XRDS>
				<?php
				$xrds=ob_get_contents();
				ob_end_clean();
				
				header("Content-type: text/xml");
				echo $xrds;
				JFactory::getApplication()->close();
				break;
			// Registration
			case 4:
				$db=JFactory::getDBO();
				
				/*$f=fopen(JPATH_SITE.'/tmp/mojeid_register.txt','a+');
				fwrite($f,print_r($_REQUEST,1));
				fclose($f);*/
				
				$user=JUser::getInstance();
				$user->load(base64_decode(JRequest::getVar('registration_nonce')));
				
				if(empty($user->id)) {
					$username=JRequest::getVar('claimed_id');
					$username=str_replace(array("http://","https://",".fred.nic.cz",'.mojeid'),"",$username);
					$username=preg_replace('@/#(.*)$@','',$username);
					
					$user->set('username',$username);
					$user->set('name',$username);
					$user->set('email',$username.'@'.$username.'.nic');
					$user->set('groups',array(JComponentHelper::getParams('com_users')->get('new_usertype',2)));	
				}
				
				if(!$user->save()) {
					?>
					mode: reject
					reason: <?php echo $user->getError(); ?>
					<?php
				} else {
					?>
					mode: accept
					
					<?php
				}
				$query=$db->getQuery(true);
				$query->insert('#__user_mojeid');
				$query->set('user_id='.$user->id);
				$query->set('mojeid_ident='.$db->quote(JRequest::getVar('claimed_id')));
				$query.=" ON DUPLICATE KEY UPDATE mojeid_ident=".$db->quote(preg_replace('@/#(.*)$@','',JRequest::getVar('claimed_id')));
				$db->setQuery($query);
				$db->execute();
				JFactory::getApplication()->close();
				break;
			// Unbind account
			case 5:
				$db=JFactory::getDBO();
				$query=$db->getQuery(true);
				$query->delete('#__user_mojeid');
				$query->where('user_id='.JFactory::getUser()->id);
				$db->setQuery($query);
				$db->execute();
				
				$this->_sendPassword();
				
				JFactory::getApplication()->enqueueMessage(JText::_('PLG_SYSTEM_MOJEID_ACCOUNT_SUCCESSFULLY_UNBOUNDED'));
				JFactory::getApplication()->redirect('index.php');
				break;
		}
	}
	
	function onAfterRender() {
		if(JFactory::getApplication()->isAdmin()) {
			return true;
		}
		
		if($this->params->get('test_mode',0)==1) {
			$registration_endpoint="https://mojeid.fred.nic.cz/registration/endpoint/";
		} else {
			$registration_endpoint="https://mojeid.cz/registration/endpoint/";
		}
		
		$body=JResponse::getBody();
		$body=str_replace('{endpointform}',$registration_endpoint,$body);
		JResponse::setBody($body);
		return true;
	}
	
	private function _getLogin() {
		require_once JPATH_LIBRARIES.'/lib_mojeid_openid_j25/common.php';
		
		if($this->params->get('test_mode',0)==1) {
			$endpoint="https://mojeid.fred.nic.cz/endpoint/";
		} else {
			$endpoint="https://mojeid.cz/endpoint/";
		}
		$consumer=getConsumer();
		
		$auth=$consumer->begin($endpoint);
		if(!$auth) {
			$this->setError(JText::_('PLG_SYSTEM_MOJEID_LOGIN_NOT_VALID_OPEN_ID'));
			return false;
		}
		
		$sreg_request = Auth_OpenID_SRegRequest::build(array('fullname', 'email'), array());
		if($sreg_request) {
			$auth->addExtension($sreg_request);
		}
		
		$pape=new Auth_OpenID_PAPE_Request(array());
		if($pape) {
			$auth->addExtension($pape);
		}
		$ax=new Auth_OpenID_AX_FetchRequest();
		
		$ax_uris_req=array("http://specs.nic.cz/attr/contact/adult","http://specs.nic.cz/attr/contact/valid");
		foreach($ax_uris_req as $i=>$uri) {
			$ax->add(new Auth_OpenID_AX_AttrInfo($uri,1,true,"axr".$i));
		}
		if(JPluginHelper::isEnabled('user','profile')) {
			$ax_uris=array("http://axschema.org/contact/postalAddress/home","http://axschema.org/contact/city/home","http://axschema.org/contact/postalCode/home","http://axschema.org/contact/phone/default",
				"http://axschema.org/contact/phone/home","http://axschema.org/contact/web/default","http://specs.nic.cz/attr/contact/ident/dob"
			);
			foreach($ax_uris as $i=>$uri) {
				$ax->add(new Auth_OpenID_AX_AttrInfo($uri,1,false,"ax_profile".$i));
			}
		}
		if(JComponentHelper::isEnabled('com_virtuemart')) {
			$ax_uris=array("http://specs.nic.cz/attr/addr/bill/street","http://specs.nic.cz/attr/addr/bill/city","http://specs.nic.cz/attr/addr/bill/pc","http://specs.nic.cz/attr/addr/bill/cc",
				"http://specs.nic.cz/attr/addr/ship/street","http://specs.nic.cz/attr/addr/ship/city","http://specs.nic.cz/attr/addr/ship/pc","http://specs.nic.cz/attr/addr/ship/cc"
			);
			foreach($ax_uris as $i=>$uri) {
				$ax->add(new Auth_OpenID_AX_AttrInfo($uri,1,false,"ax_virtuemart".$i));
			}
		}
		if(JComponentHelper::isEnabled('com_spaceshop')) {
			$ax_uris=array("http://specs.nic.cz/attr/addr/bill/street","http://specs.nic.cz/attr/addr/bill/city","http://specs.nic.cz/attr/addr/bill/pc","http://specs.nic.cz/attr/addr/bill/cc",
				"http://specs.nic.cz/attr/contact/ident/vat_id","http://specs.nic.cz/attr/contact/vat"
			);
			foreach($ax_uris as $i=>$uri) {
				$ax->add(new Auth_OpenID_AX_AttrInfo($uri,1,false,"ax_spaceshop".$i));
			}
		}
		$auth->addExtension($ax);
		
		if($auth->shouldSendRedirect()) {
			$url=$auth->redirectURL(getTrustRoot(),getReturnTo());
			if(Auth_OpenID::isFailure($url)) {
				$this->setError(JText::sprintf('PLG_SYSTEM_MOJEID_LOGIN_COULDNT_REDIRECT',$url->message));
				return false;
			}
			JFactory::getApplication()->redirect($url);
			JFactory::getApplication()->close();
		} else {
			$id='mojeid_msg';
			$html=$auth->htmlMarkup(getTrustRoot(),getReturnTo(),false,array('id'=>$id));
			if(Auth_OpenID::isFailure($html)) {
				$this->setError(JText::sprintf('PLG_SYSTEM_MOJEID_LOGIN_COULDNT_REDIRECT',$html->message));
				return false;
			}
			return $html;
		}
	}
	
	private function _sendPassword() {
		$db=JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select('password');
		$query->from('#__users');
		$query->where('id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$password=$db->loadResult();
		if(!empty($password)) {
			return true;
		}
		
		$password=JUserHelper::genRandomPassword();
		
		$user=JUser::getInstance();
		$user->load(JFactory::getUser()->id);
		$user->password=JUserHelper::hashPassword($password);
		if(!$user->save()) {
			$this->setError($user->getError());
			return false;
		}
		
		$mail=JFactory::getMailer();
		$mail->setSender(array(JFactory::getApplication()->getCfg('mailfrom'),JFactory::getApplication()->getCfg('from_name')));
		$mail->addRecipient(JFactory::getUser()->email);
		$mail->setSubject(JText::sprintf('PLG_SYSTEM_MOJEID_PASSWORD_SUBJECT',JFactory::getApplication()->getCfg('sitename')));
		
		$body=JText::_('PLG_SYSTEM_MOJEID_PASSWORD_BODY');
		$vars=array(
			'{username}'=>JFactory::getUser()->username,
			'{name}'=>JFactory::getUser()->name,
			'{password}'=>$password
		);
		foreach($vars as $var=>$val) {
			$body=str_replace($var,$val,$body);
		}
		
		$mail->setBody($body);
		$mail->Send();
		return true;
	}
}
?>