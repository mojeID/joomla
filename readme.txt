Poznámky k vydání mojeID pro Joomla 2.5 a 3.3
---------------------------------------------

Vývoj rozšíření zajišťuje firma WebSite21
-----------------------------------------
info@website21.cz | www.website21.cz

Postup instalace:

- Celý projekt stáheneme jako zip, abychom vytvořili balíček (edit 4.11.2015 Edvard Rejthar)
- Balíček "pkg_mojeid_j25_j3x.zip" instalujeme pomocí nativního instalátoru Joomla!
- Pluginy v balíčku s znázvem "_plg_system_mojeid_j25" a "_plg_authentication_mojeid_j25" jsou po instalaci ve výchozím nastavení deaktivovány.

