<?php
/**
* Created on Mar 7, 2013
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.folder');

class plgAuthenticationMojeID extends JPlugin {
	function __construct($config,$params) {
		parent::__construct($config,$params);
		$this->loadLanguage();
	}
	
	function onUserAuthenticate(&$credentials,$options,&$response) {
		require_once JPATH_LIBRARIES.'/lib_mojeid_openid_j25/common.php';
		
		$consumer=getConsumer();
		$result=$consumer->complete(getReturnTo());
		
		switch($result->status) {
			case Auth_OpenID_CANCEL:
				$response->status=JAuthentication::STATUS_CANCEL;
				$response->error_message=JText::_('PLG_AUTHENTICATE_MOJEID_AUTHENTICATION_CANCELLED');
				break;
			case Auth_OpenID_FAILURE:
				/*$response->status=JAuthentication::STATUS_FAILURE;
				$response->error_message=JText::sprintf('PLG_AUTHENTICATE_MOJEID_AUTHENTICATION_FAILED',$result->message);*/
				break;
			case Auth_OpenID_SUCCESS:
				$response->status=JAuthentication::STATUS_SUCCESS;
				$response->error_message="";
				
				if(!$this->_saveUser($response,$result)) {
					return false;
				}
				
				break;
		}
	}
	
	private function _saveUser(&$response,$result) {
		$db=JFactory::getDBO();
				
		$query=$db->getQuery(true);
		$query->select('u.username');
		$query->from('#__user_mojeid AS um');
		$query->leftJoin('#__users AS u ON um.user_id=u.id');
		$query->where('mojeid_ident='.$db->quote(htmlentities($result->getDisplayIdentifier())));
		$db->setQuery($query);
		$response->username=$db->loadResult();
		if(empty($response->username)) {
			if(empty(JFactory::getUser()->id)) {
				$response->username=htmlentities($result->getDisplayIdentifier());
				$response->username=str_replace(array("http://","https://","/"),"",$response->username);
				$response->username=str_replace(array(".fred.nic.cz"),"",$response->username);
				$response->username=str_replace('.mojeid','',$response->username);
			} else {
				$response->username=JFactory::getUser()->username;
			}	
		}
		
		$data_resp=Auth_OpenID_SRegResponse::fromSuccessResponse($result);
		$data=$data_resp->contents();
		if(isset($data["email"])) {
			$response->email=$data["email"];
		} else {
			$response->email=str_replace(array("http://","https://"),"",$response->username);
			$response->email=str_replace('/','-',$response->email);
			$response->email.='@mojeid.';
		}
		$response->fullname=isset($data["fullname"])?$data["fullname"]:$response->username;
		$response->language=isset($data["language"])?$data["language"]:null;
		$response->timezone=isset($data["timezone"])?$data["timezone"]:null;
		
		$ax=Auth_OpenID_AX_FetchResponse::fromSuccessResponse($result);
		
		if($this->params->get('adult_protect',0)==1) {
			if(!isset($ax->data["http://specs.nic.cz/attr/contact/valid"][0]) || strtolower($ax->data["http://specs.nic.cz/attr/contact/valid"][0])=="false") {
				$response->status=JAuthentication::STATUS_FAILURE;
				$response->error_message=JText::_('PLG_AUTHENTICATE_MOJEID_AUTHENTICATION_FAILED_PROFILE_NOT_VALIDATED');
				return false;					
			} else if(strtolower($ax->data["http://specs.nic.cz/attr/contact/adult"][0])=="false") {
				$response->status=JAuthentication::STATUS_FAILURE;
				$response->error_message=JText::_('PLG_AUTHENTICATE_MOJEID_AUTHENTICATION_FAILED_AGE');
				return false;
			}
		}
		
		$user=JUser::getInstance();
		if(JFactory::getUser()->id>0) {
			$user->load(JFactory::getUser()->id);
		} else {
			if(($user_id=JUserHelper::getUserId($response->username))) {
				$user->load($user_id);	
			}
			$user->set('username',$response->username);	
		}
		
		$user->set('email',$response->email);
		$user->set('name',$response->fullname);
		if($user->get('id')==0) {
			$user->set('groups',array(JComponentHelper::getParams('com_users')->get('new_usertype',2)));
		}
		if(!$user->save()) {
			$response->status=JAuthentication::STATUS_FAILURE;
			$response->error_message=JText::sprintf('PLG_AUTHENTICATE_MOJEID_AUTHENTICATION_FAILED',$user->getError());
			return false;
		}
		
		$query=$db->getQuery(true);
		$query->insert('#__user_mojeid');
		$query->set('user_id='.$user->id);
		$query->set('mojeid_ident='.$db->quote($result->getDisplayIdentifier()));
		$query.=" ON DUPLICATE KEY UPDATE mojeid_ident=".$db->quote($result->getDisplayIdentifier());
		$db->setQuery($query);
		$db->execute();
		
		if(JPluginHelper::isEnabled('user','profile')) {
			$ax_data=$ax->data;
			$data=array();
			if(!empty($ax_data['http://axschema.org/contact/city/home'][0])) {
				$data['city']=$ax_data['http://axschema.org/contact/city/home'][0];
			}
			if(!empty($ax_data['http://axschema.org/contact/postalAddress/home'][0])) {
				$data['address1']=$ax_data['http://axschema.org/contact/postalAddress/home'][0];
			}
			if(!empty($ax_data['http://axschema.org/contact/postalCode/home'][0])) {
				$data['postal_code']=$ax_data['http://axschema.org/contact/postalCode/home'][0];
			}
			if(!empty($ax_data['http://axschema.org/contact/phone/default'][0])) {
				$data['phone']=$ax_data['http://axschema.org/contact/phone/default'][0];
			} else if(!empty($ax_data['http://axschema.org/contact/phone/home'][0])) {
				$data['phone']=$ax_data['http://axschema.org/contact/phone/home'][0];
			}
			if(!empty($ax_data['http://axschema.org/contact/web/default'][0])) {
				$data['website']=$ax_data['http://axschema.org/contact/web/default'][0];
			}
			if(!empty($ax_data['http://specs.nic.cz/attr/contact/ident/dob'][0])) {
				$data['dob']=$ax_data['http://specs.nic.cz/attr/contact/ident/dob'][0];
			}
			
			
			foreach($data as $key=>$val) {
				$query=$db->getQuery(true);
				$query->select('COUNT(*)');
				$query->from('#__user_profiles');
				$query->where('user_id='.$user->get('id'));
				$query->where('profile_key='.$db->quote('profile.'.$key));
				$db->setQuery($query);
				$new=!(bool)$db->loadResult();
			
				$query=$db->getQuery(true);
				if($new) {
					$query->insert('#__user_profiles');
					$query->set('user_id='.$user->get('id'));
				} else {
					$query->update('#__user_profiles');
				}
				$query->set('profile_key='.$db->quote('profile.'.$key));
				$query->set('profile_value='.$db->quote(json_encode($val)));			
				
				if(!$new) {
					$query->where('user_id='.$user->get('id'));	
					$query->where('profile_key='.$db->quote('profile.'.$key));
				}
				$db->setQuery($query);
				if(!$db->query()) {
					$this->setError($db->stderr(true));
					return false;
				}
			}
		}
		
		if(JFolder::exists(JPATH_ADMINISTRATOR.'/components/com_virtuemart')) {
			if(!$this->_saveVirtueMart($user,$response,$result)) { 
				return false;
			}
		}
		if(JFolder::exists(JPATH_ADMINISTRATOR.'/components/com_spaceshop')) {
			if(!$this->_saveSPACEShop($user,$response,$result)) { 
				return false;
			}
		}
		return true;
	}
	
	private function _saveVirtueMart($user,&$response,$result) {
		$db=JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select('COUNT(*)');
		$query->from('#__virtuemart_userinfos');
		$query->where('virtuemart_user_id='.$user->get('id'));
		$query->where('address_type='.$db->quote('BT'));
		$db->setQuery($query);
		$bt_new=!(bool)$db->loadResult();
		
		$query=$db->getQuery(true);
		$query->select('COUNT(*)');
		$query->from('#__virtuemart_userinfos');
		$query->where('virtuemart_user_id='.$user->get('id'));
		$query->where('address_type='.$db->quote('ST'));
		$db->setQuery($query);
		$st_new=!(bool)$db->loadResult();
		
		$bt_query=$db->getQuery(true);
		if($bt_new) {
			$bt_query->insert('#__virtuemart_userinfos');
			$bt_query->set('virtuemart_user_id='.$user->get('id'));
			$bt_query->set('address_type='.$db->quote('BT'));
		} else {
			$bt_query->update('#__virtuemart_userinfos');
		}
		
		$st_query=$db->getQuery(true);
		if($st_new) {
			$st_query->insert('#__virtuemart_userinfos');
			$st_query->set('virtuemart_user_id='.$user->get('id'));
			$st_query->set('address_type='.$db->quote('ST'));
		} else {
			$st_query->update('#__virtuemart_userinfos');
		}
		
		$name=explode(" ",$response->fullname);
		$bt_query->set('first_name='.$db->quote($name[0]));
		$bt_query->set('last_name='.$db->quote($name[1]));
		
		$st_query->set('first_name='.$db->quote($name[0]));
		$st_query->set('last_name='.$db->quote($name[1]));
		$st_query->set('address_type_name='.$db->quote('MojeID'));
		
		$ax=Auth_OpenID_AX_FetchResponse::fromSuccessResponse($result);
		foreach($ax->data as $uri=>$vals) {
			$name=str_replace("http://specs.nic.cz/attr/","",$uri);
			switch($name) {
				case 'addr/bill/street':
					if(isset($vals[0])) {
						$bt_query->set('address_1='.$db->quote($vals[0]));
					}
					break;
				case 'addr/bill/city':
					if(isset($vals[0])) {
						$bt_query->set('city='.$db->quote($vals[0]));
					}
					break;
				case 'addr/bill/pc':
					if(isset($vals[0])) {
						$bt_query->set('zip='.$db->quote($vals[0]));	
					}
					break;
				case 'addr/bill/cc':
					if(isset($vals[0])) {
						$country_query=$db->getQuery(true);
						$country_query->select('virtuemart_country_id');
						$country_query->from('#__virtuemart_countries');
						$country_query->where('country_2_code='.$db->quote($vals[0]));
						$db->setQuery($country_query);
						$bt_query->set('virtuemart_country_id='.$db->loadResult());
					}
					break;
				case 'http://axschema.org/contact/phone/default':
					if(!empty($vals[0])) {
						$bt_query->set('phone_1='.$db->quote($vals[0]));
					}
					break;
				case 'http://axschema.org/contact/phone/home':
					if(!empty($vals[0])) {
						$bt_query->set('phone_1='.$db->quote($vals[0]));
					}
					break;
												
				case 'addr/ship/street':
					if(isset($vals[0])) {
						$st_query->set('address_1='.$db->quote($vals[0]));
					}
					break;
				case 'addr/ship/city':
					if(isset($vals[0])) {
						$st_query->set('city='.$db->quote($vals[0]));
					}
					break;
				case 'addr/ship/pc':
					if(isset($vals[0])) {
						$st_query->set('zip='.$db->quote($vals[0]));
					}
					break;		
				case 'addr/ship/cc':
					if(isset($vals[0])) {
						$country_query=$db->getQuery(true);
						$country_query->select('virtuemart_country_id');
						$country_query->from('#__virtuemart_countries');
						$country_query->where('country_2_code='.$db->quote($vals[0]));
						$db->setQuery($country_query);
						$st_query->set('virtuemart_country_id='.$db->loadResult());
					}
				
			}
		}
		if(!$bt_new) {
			$bt_query->where('virtuemart_user_id='.$user->get('id'));	
			$bt_query->where('address_type='.$db->quote('BT'));
		}
		$db->setQuery($bt_query);
		if(!$db->query()) {
			$this->setError($db->stderr(true));
			return false;
		}
		
		if(!$st_new) {
			$st_query->where('virtuemart_user_id='.$user->get('id'));
			$st_query->where('address_type='.$db->quote('ST'));	
		}
		$db->setQuery($st_query);
		if(!$db->query()) {
			$this->setError($db->stderr(true));
			return false;
		}
		return true;
	}
	
	private function _saveSPACEShop($user,&$response,$result) {
		$name=explode(" ",$response->fullname);
		
		$fields=array();
		$fields['firstname']=$name[0];
		$fields['surname']=$name[1];
		
		$ax=Auth_OpenID_AX_FetchResponse::fromSuccessResponse($result);
		foreach($ax->data as $uri=>$vals) {
			$name=str_replace("http://specs.nic.cz/attr/","",$uri);
			switch($name) {
				case 'addr/bill/street':
					if(isset($vals[0])) {
						$fields['street']=$vals[0];
					}
					break;
				case 'addr/bill/city':
					if(isset($vals[0])) {
						$fields['city']=$vals[0];
					}
					break;
				case 'addr/bill/pc':
					if(isset($vals[0])) {
						$fields['postcode']=$vals[0];
					}
					break;
				case 'addr/bill/cc':
					if(isset($vals[0])) {
						$query=$db->getQuery(true);
						$query->select('id');
						$query->from('#__ss_countries');
						$query->where('country_2_code='.$db->quote($vals[0]));
						$db->setQuery($query);
						$fields['country_id']=$db->loadResult();
					}
					break;							
				case 'contact/ident/vat_id':
					if(isset($vals[0])) {
						$fields['ico']=$vals[0];
					}
					break;
				case 'contact/vat':
					if(isset($vals[0])) {
						$fields['dic']=$vals[0];
					}
					break;
						
			}
		}
		
		foreach($fields as $key=>$val) {
			$query=$db->getQuery(true);
			$query->select('COUNT(*)');
			$query->from('#__user_profiles');
			$query->where('user_id='.$user->get('id'));
			$query->where('profile_key='.$db->quote('spaceshop.'.$key));
			$db->setQuery($query);
			$new=!(bool)$db->loadResult();
		
			$query=$db->getQuery(true);
			if($new) {
				$query->insert('#__user_profiles');
				$query->set('user_id='.$user->get('id'));
			} else {
				$query->update('#__user_profiles');
			}
			$query->set('profile_key='.$db->quote('spaceshop.'.$key));
			$query->set('profile_value='.$db->quote(json_encode($val)));			
			
			if(!$new) {
				$query->where('user_id='.$user->get('id'));	
				$query->where('profile_key='.$db->quote('spaceshop.'.$key));
			}
			$db->setQuery($query);
			if(!$db->query()) {
				$this->setError($db->stderr(true));
				return false;
			}
		}
		return true;
	}
}
?>