<?php
/**
* Created on Aug 21, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

class MojeIDHelperLogin extends JObject {
	private $_db=null;
	
	function __construct() {
		$this->_db=JFactory::getDBO();
	}
	
	function getUserProfile() {
		$query=$this->_db->getQuery(true);
		$query->select('REPLACE(profile_key,"profile.","") AS `key`, profile_value AS val');
		$query->from('#__user_profiles');
		$query->where('user_id='.JFactory::getUser()->id);
		$query->where('profile_key LIKE '.$this->_db->quote('profile.%'));
		$this->_db->setQuery($query);
		return $this->_db->loadObjectList('key');		
	}
	
	function getHasBind() {
		$query=$this->_db->getQuery(true);
		$query->select('COUNT(*)');
		$query->from('#__user_mojeid');
		$query->where('user_id='.JFactory::getUser()->id);
		$this->_db->setQuery($query);
		return (bool)$this->_db->loadResult();
	}
	
	function getMojeIdIdentity() {
		$query=$this->_db->getQuery(true);
		$query->select('mojeid_ident');
		$query->from('#__user_mojeid');
		$query->where('user_id='.JFactory::getUser()->id);
		$this->_db->setQuery($query);
		return str_replace(array("https://","/"),"",$this->_db->loadResult());
	}
}
?>