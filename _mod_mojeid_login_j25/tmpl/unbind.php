<?php
/**
* Created on Sep 9, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');


?>
<div class="mojeidlogin<?php echo $params->get('moduleclass_sfx'); ?>">
	<?php echo JText::_('MOD_MOJEID_LOGIN_MOJEID_IDENTITY'); ?>:&nbsp;<?php echo $mojeid_identity; ?>
	<div>
		<a href="index.php?mojeid_login=5" class="button btn btn-primary"><?php echo JText::_('MOD_MOJEIDLOGIN_UNBIND'); ?></a>
	</div>
	<div>
		<a href="index.php?option=com_mojeid&view=localaccount" class="button btn btn-primary"><?php echo JText::_('MOD_MOJEIDLOGIN_BIND_TO_LOCAL_ACCOUNT'); ?></a>
	</div>
</div>