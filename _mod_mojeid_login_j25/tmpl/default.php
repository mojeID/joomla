<?php
/**
* Created on Mar 7, 2013
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

?>
<style type="text/css">
	.login, .register {
		margin-bottom:10px;
	}
	.register input {
		border:0px; 
		background: url("<?php echo JFactory::getURI()->base(); ?>modules/mod_mojeid_login/assets/BLACK/zalozit_grey_3D.png") repeat scroll 0 0 rgba(0, 0, 0, 0);
		border: 0 none;
		font-size: 0; 
		height: 32px;
    	width: 162px;
    	cursor:poiner
	}
</style>
<div class="mojeidlogin<?php echo $params->get('moduleclass_sfx'); ?>">
	<div class="login">
		<?php echo JHTML::_('link',JRoute::_('index.php?mojeid_login=1',true),JHTML::_('image',JFactory::getURI()->base().'/modules/mod_mojeid_login/assets/BLACK/162x32.png',JText::_('MOD_MOJEID_LOGIN')));; ?>
	</div>
	<div class="register">
		<form action="{endpointform}" method="post" name="mojeIdRegisterForm" id="mojeIdRegisterForm">
			<input type="submit" value="<?php echo JText::_('MOD_MOJEIDLOGIN_REGISTER'); ?>" />
			<input type="hidden" name="realm" value="<?php echo JFactory::getURI()->base(); ?>" />
			<input type="hidden" name="registration_nonce" value="<?php echo JSession::getInstance('none',array())->getToken(); ?>" />
		</form>
	</div>
	<div class="link">
		<?php echo JHTML::_('link','https://www.mojeid.cz/vyhody/',JText::_('MOD_MOJEIDLOGIN_WHY')); ?>
	</div>
</div>