<?php
/**
* Created on Mar 7, 2013
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

if(JFactory::getUser()->get('id')==0) {
	require JModuleHelper::getLayoutPath('mod_mojeid_login','default');
} else {
	require_once __DIR__.'/helper.php';
	$helper=new MojeIDHelperLogin();
	if(!$helper->getHasBind()) {
		$profile=$helper->getUserProfile();
		require JModuleHelper::getLayoutPath('mod_mojeid_login','bind');	
	} else {
		$mojeid_identity=$helper->getMojeIdIdentity();
		require JModuleHelper::getLayoutPath('mod_mojeid_login','unbind');
	}
}
?>