<?php
/**
* Created on Sep 25, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class MojeIDViewAccounts extends JViewLegacy {
	function display($tpl=null) {
		$this->state=$this->get('state');
		$this->items=$this->get('items');
		$this->pagination=$this->get('pagination');
		$this->filterForm=$this->get('FilterForm');
		$this->activeFilters=$this->get('ActiveFilters');
		
		JToolbarHelper::title(JText::_('COM_MOJEID_ACCOUNTS'));
		/*if(JFactory::getUser()->authorise('core.create','com_mojeid')) {
			JToolbarHelper::addNew('account.add');
		}
		if(JFactory::getUser()->authorise('core.edit','com_mojeid')) {
			JToolbarHelper::editList('account.edit');
		}
		if(JFactory::getUser()->authorise('core.delete	','com_mojeid')) {
			JToolbarHelper::deleteList('','accounts.delete');
		}*/
		
		//JHtml::_('formbehavior.chosen','select');
		
		parent::display($tpl);
	}
}
?>