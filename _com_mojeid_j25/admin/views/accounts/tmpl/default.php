<?php
/**
* Created on Sep 25, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');


?>
<form action="<?php echo JRoute::_('index.php?option=com_mojeid&view=accounts'); ?>" method="post" name="adminForm" id="adminForm">
    <div id="j-main-container" class="span12">
		<div class="filter-search btn-group pull-left">
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->state->get('filter.search'); ?>" class="hasTooltip" onchange="this.form.submit();"/>
		</div>
		<div class="btn-group pull-left">
				<button type="submit" class="btn hasTooltip"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
		</div>
		<div class="btn-group pull-right">
				<?php echo $this->pagination->getLimitBox(); ?>
		</div>
		<div class="clearfix"></div>
		<table class="adminlist table table-striped" id="accountsList">
			<thead>
				<tr>
					<!--<th width="1%" class="hidden-phone">
						<?php //echo JHtml::_('grid.checkall'); ?>
					</th>-->
					<th class="title">
						<?php echo JHTML::_('grid.sort','COM_MOJEID_IDENTITY','um.mojeid_ident',$this->state->get('list.direction'),$this->state->get('list.ordering')); ?>
					</th>
					<th>
						<?php echo JHTML::_('grid.sort','COM_MOJEID_USERNAME','u.username',$this->state->get('list.direction'),$this->state->get('list.ordering')); ?>
					</th>
					<th>
						<?php echo JHTML::_('grid.sort','COM_MOJEID_NAME','u.name',$this->state->get('list.direction'),$this->state->get('list.ordering')); ?>
					</th>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo Jtext::_('JGRID_HEADING_ID'); ?>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($this->items as $i=>$item) {
					?>
					<tr>
						
						<!--<td class="hidden-phone"><?php echo JHTML::_('grid.id',$i,$item->id); ?></td>-->
						<td class="nowrap has-context"><?php echo $item->title; ?></td>
						<td><?php echo $item->username; ?></td>
						<td><?php echo $item->name; ?></td>
						<td class="center hidden-phone"><?php echo $item->id; ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="99">
					<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		</table>
		<input type="hidden" name="option" value="com_mojeid" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $this->state->get('list.ordering'); ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->state->get('list.direction'); ?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHTML::_('form.token'); ?>
	</div>
</form>