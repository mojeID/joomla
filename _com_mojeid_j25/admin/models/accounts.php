<?php
/**
* Created on Sep 25, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class MojeIDModelAccounts extends JModelList {
	function __construct($config=array()) {
		parent::__construct($config);
	}
	
	function populateState($ordering=null,$direction=null) {
		$this->setState('filter.search',$this->getUserStateFromRequest($this->context.'.filter.search','filter_search','','string'));
		parent::populateState($ordering,$direction);
		$this->setState('list.ordering',$this->getUserStateFromRequest($this->context.'.list.ordering','filter_order','u.name','string'));
		$this->setState('list.direction',$this->getUserStateFromRequest($this->context.'.list.direction','filter_order_Dir','ASC','string'));
	}
	
	protected function getListQuery() {
		$query=$this->_db->getQuery(true);
		$query->select('um.mojeid_ident AS title, u.username, u.name, u.id');
		$query->from('#__user_mojeid AS um');
		$query->innerJoin('#__users AS u ON um.user_id=u.id');
		if(strlen($this->getState('filter.search'))>0) {
			$where_fields=array('um.mojeid_ident','u.username','u.name');
			$where=array();
			foreach($where_fields as $field) {
				$where[]='(LOWER('.$field.') LIKE ('.$this->_db->quote('%'.strtolower($this->getState('filter.search')).'%').'))';
			}
			$query->where('('.implode(" OR ",$where).')');
		}
		$query->order($this->getState('list.ordering','u.name').' '.$this->getState('list.direction','ASC'));
		return $query;
	}
}
?>