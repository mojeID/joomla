<?php
/**
* Created on Sep 24, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class MojeIDControllerLocalAccount extends JControllerLegacy {
	function getModel($type='LocalAccount',$prefix='MojeIDModel',$config=array('ignore_request'=>true)) {
		return parent::getModel($type,$prefix,$config);
	} 
	
	function bind() {
		JSession::checkToken() or jexit('JINVALID_TOKEN');
		
		$request=JRequest::get('request');
		$model=$this->getModel();
		$this->setRedirect('index.php');
		
		if(!$model->bind($request)) {
			$this->setMessage($model->getError(),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_mojeid&view=localaccount',false));
			return false;
		}
		$this->setMessage(JText::_('COM_MOJEID_LOCAL_ACCOUNT_SUCCESSULLY_BOUND_TO_MOJEID'));
	}
}
?>