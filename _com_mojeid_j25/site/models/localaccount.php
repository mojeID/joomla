<?php
/**
* Created on Sep 24, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class MojeIDModelLocalAccount extends JModelLegacy  {
	function bind($request) {
		jimport('joomla.user.authentication');
		
		if(JFactory::getUser()->id==0) {
			return false;
		}
		
		$auth=JAuthentication::getInstance();
		$status=$auth->authenticate($request);
		if($status->status!=JAuthentication::STATUS_SUCCESS) {
			$this->setError($status->error_message);
			return false;
		}
	
		$authorisations=$auth->authorise($status);
		foreach($authorisations as $authorisation) {
			if(in_array($authorisation->status,array(JAuthentication::STATUS_EXPIRED,JAuthentication::STATUS_DENIED))) {
				switch ($authorisation->status) {
	                case JAuthentication::STATUS_EXPIRED:
	                    $this->setError(JText::_('JLIB_LOGIN_EXPIRED'));
	                    break;
	                case JAuthentication::STATUS_DENIED:
	                    $this->setError(JText::_('JLIB_LOGIN_DENIED'));
	                    break;
	                default:
	                    $this->setError(JText::_('JLIB_LOGIN_AUTHORISATION'));
	                    break;
	 			}
				return false;
			}	
		}
		
		
		$mojeid_user_id=JFactory::getUser()->id;
		
		$local_account_user_id=JUserHelper::getUserId($request['username']);
		
		$local_user=JUser::getInstance();
		$local_user->load($local_account_user_id);
		
		$mojeid_user=JUser::getInstance();
		$mojeid_user->load($mojeid_user_id);
		$mojeid_user->username=$local_user->username;
		$mojeid_user->password=$local_user->password;
		$mojeid_user->params=$local_user->params;
		$mojeid_user->groups=$local_user->groups;
		
		if(JPluginHelper::isEnabled('user','profile')) {
			$query=$this->_db->getQuery(true);
			$query->select('profile_key AS `key`, profile_value AS `value`');
			$query->from('#__user_profiles');
			$query->where('user_id='.$local_account_user_id);
			$query->where('profile_key LIKE '.$this->_db->quote('profile.%'));
			$this->_db->setQuery($query);
			$local_profile=$this->_db->loadObjectList();
			
			foreach($local_profile as $item) {
				$query=$this->_db->getQuery(true);
				$query->select('profile_value');
				$query->from('#__user_profiles');
				$query->where('user_id='.$mojeid_user_id);
				$query->where('profile_key='.$this->_db->quote($item->key));
				$this->_db->setQuery($query);
				$mojeid_value=json_decode($this->_db->loadResult());
				if(empty($mojeid_value)) {
					$query=$this->_db->getQuery(true);
					$query->select('COUNT(*)');
					$query->from('#__user_profiles');
					$query->where('user_id='.$mojeid_user_id);
					$query->where('profile_key='.$this->_db->quote($item->key));
					$this->_db->setQuery($query);
					$new=!(bool)$this->_db->loadResult();
				
					$query=$this->_db->getQuery(true);
					if($new) {
						$query->insert('#__user_profiles');
						$query->set('profile_key='.$this->_db->quote($item->key));
						$query->set('user_id='.$mojeid_user_id);
					} else {
						$query->update('#__user_profiles');
						$query->where('profile_key='.$this->_db->quote($item->key));
						$query->where('user_id='.$mojeid_user_id);	
					}
					$query->set('profile_value='.$this->_db->quote($item->value));
					$this->_db->setQuery($query);
					if(!$this->_db->query()) {
						$this->setError($this->_db->stderr(true));
						return false;
					}
				}
			}
			
			$query=$this->_db->getQuery(true);
			$query->delete('#__user_profiles');
			$query->where('user_id='.$local_account_user_id);
			$query->where('profile_key LIKE '.$this->_db->quote('profile.%'));
			$this->_db->setQuery($query);
			if(!$this->_db->query()) {
				$this->setError($this->_db->stderr(true));
				return false;
			}
		}

		if(JFolder::exists(JPATH_ADMINISTRATOR.'/components/com_virtuemart')) {
			$query=$this->_db->getQuery(true);
			$query->select('*');
			$query->from('#__virtuemart_userinfos');
			$query->where('address_type='.$this->_db->quote('BT'));
			$query->where('virtuemart_user_id='.$local_account_user_id);
			$this->_db->setQuery($query);
			$local_bt_addr=$this->_db->loadObject();
			unset($local_bt_addr->virtuemart_userinfo_id);
			unset($local_bt_addr->virtuemart_user_id);
			unset($local_bt_addr->created_on);
			unset($local_bt_addr->created_by);
			unset($local_bt_addr->modified_on);
			unset($local_bt_addr->modified_by);
			unset($local_bt_addr->locked_on);
			unset($local_bt_addr->locked_by);
			
			if(!empty($local_bt_addr)) {
				$query=$this->_db->getQuery(true);
				$query->select('*');
				$query->from('#__virtuemart_userinfos');
				$query->where('address_type='.$this->_db->quote('BT'));
				$query->where('virtuemart_user_id='.$mojeid_user_id);
				$this->_db->setQuery($query);
				$mojeid_bt_addr=$this->_db->loadObject();
				
				$query=$this->_db->getQuery(true);
				$query->select('COUNT(*)');
				$query->from('#__virtuemart_userinfos');
				$query->where('address_type='.$this->_db->quote('BT'));
				$query->where('virtuemart_user_id='.$mojeid_user_id);
				$this->_db->setQuery($query);
				$bt_addr_is_new=!(bool)$this->_db->loadResult();
				
				var_dump($bt_addr_is_new);
				
				$query=$this->_db->getQuery(true);
				if($bt_addr_is_new) {
					$query->insert('#__virtuemart_userinfos');
					$query->set('virtuemart_user_id='.$mojeid_user_id);
				} else {
					$query->update('#__virtuemart_userinfos');
					$query->where('virtuemart_user_id='.$mojeid_user_id);
				}
				foreach(get_object_vars($local_bt_addr) as $key=>$val) {
					if(empty($mojeid_bt_addr->$key)) {
						$query->set($key.'='.$this->_db->quote($val));
					}
				}
				$query->where('address_type='.$this->_db->quote('BT'));
				$this->_db->setQuery($query);
				if(!$this->_db->query()) {
					$this->setError($this->_db->stderr(true));
					return false;
				}
			}
			
			$query=$this->_db->getQuery(true);
			$query->select('*');
			$query->from('#__virtuemart_userinfos');
			$query->where('address_type='.$this->_db->quote('ST'));
			$query->where('virtuemart_user_id='.$local_account_user_id);
			$this->_db->setQuery($query);		
			$local_st_addr=$this->_db->loadObject();
			
			$query=$this->_db->getQuery(true);
			$query->select('*');
			$query->from('#__virtuemart_userinfos');
			$query->where('address_type='.$this->_db->quote('ST'));
			$query->where('virtuemart_user_id='.$mojeid_user_id);
			$this->_db->setQuery($query);
			$mojeid_st_addr=$this->_db->loadObject();
			
			if(!empty($local_st_addr)) {
				$query=$this->_db->getQuery(true);
				$query->select('COUNT(*)');
				$query->from('#__virtuemart_userinfos');
				$query->where('address_type='.$this->_db->quote('ST'));
				$query->where('virtuemart_user_id='.$mojeid_user_id);
				$this->_db->setQuery($query);
				$st_addr_is_new=!(bool)$this->_db->loadResult();
				
				$query=$this->_db->getQuery(true);
				if($st_addr_is_new) {
					$query->insert('#__virtuemart_userinfos');
					$query->set('virtuemart_user_id='.$mojeid_user_id);
				} else {
					$query->update('#__virtuemart_userinfos');
					$query->where('virtuemart_user_id='.$mojeid_user_id);
				}
				foreach(get_object_vars($local_st_addr) as $key=>$val) {
					if(empty($mojeid_st_addr->$key)) {
						$query->set($key.'='.$this->_db->quote($val));
					}
				}
				$query->where('address_type='.$this->_db->quote('ST'));
				$this->_db->setQuery($query);
				if(!$this->_db->query()) {
					$this->setError($this->_db->stderr(true));
					return false;
				}
			}
			
			$query=$this->_db->getQuery(true);
			$query->select('COUNT(*)');
			$query->from('#__virtuemart_vmusers');
			$query->where('virtuemart_user_id='.$mojeid_user_id);
			$this->_db->setQuery($query);
			$cnt=(bool)$this->_db->loadResult();
			if(!$cnt) {
				$query=$this->_db->getQuery(true);
				$query->update('#__virtuemart_vmusers');
				$query->set('virtuemart_user_id='.$mojeid_user_id);
				$query->where('virtuemart_user_id='.$local_account_user_id);
				$this->_db->setQuery($query);
				if(!$this->_db->query()) {
					$this->setError($this->_db->stderr(true));
					return false;
				}
			}
			
			$query=$this->_db->getQuery(true);
			$query->select('virtuemart_shoppergroup_id');
			$query->from('#__virtuemart_vmuser_shoppergroups');
			$query->where('virtuemart_user_id='.$local_account_user_id);
			$this->_db->setQuery($query);
			$groups_arr=$this->_db->loadColumn();
			foreach($groups_arr as $group_id) {
				$query=$this->_db->getQuery(true);
				$query->select('COUNT(*)');
				$query->from('#__virtuemart_vmuser_shoppergroups');
				$query->where('virtuemart_user_id='.$mojeid_user_id);
				$query->where('virtuemart_shoppergroup_id='.$group_id);
				$this->_db->setQuery($query);
				$cnt=$this->_db->loadResult();
				
				$query=$this->_db->getQuery(true);
				if($cnt) {
					$query->delete('#__virtuemart_vmuser_shoppergroups');
					$query->where('virtuemart_user_id='.$local_account_user_id);
					$query->where('virtuemart_shoppergroup_id='.$group_id);
					
				} else {
					$query->update('#__virtuemart_vmuser_shoppergroups');
					$query->set('virtuemart_user_id='.$mojeid_user_id);
					$query->where('virtuemart_user_id='.$local_account_user_id);
					$query->where('virtuemart_shoppergroup_id='.$group_id);
				}
				$this->_db->setQuery($query);
				if(!$this->_db->query()) {
					$this->setError($this->_db->stderr(true));
					return fakse;
				}
			}	
						
			$query=$this->_db->getQuery(true);
			$query->delete('#__virtuemart_userinfos');
			$query->where('virtuemart_user_id='.$local_account_user_id);
			$this->_db->setQuery($query);
			if(!$this->_db->query()) {
				$this->setError($this->_db->stderr(true));
				return false;
			}
		}
		if(JFolder::exists(JPATH_ADMINISTRATOR.'/components/com_spaceshop')) {
			$query=$this->_db->getQuery(true);
			$query->select('profile_key AS `key`, profile_value AS `value`');
			$query->from('#__user_profiles');
			$query->where('user_id='.$local_account_user_id);
			$query->where('profile_key LIKE '.$this->_db->quote('spaceshop.%'));
			$this->_db->setQuery($query);
			$profile=$this->_db->loadObjectList();
			
			foreach($profile as $item) {
				$query=$this->_db->getQuery(true);
				$query->select('profile_value');
				$query->from('#__user_profiles');
				$query->where('user_id='.$mojeid_user_id);
				$query->where('profile_key='.$this->_db->quote($item->key));
				$this->_db->setQuery($query);
				$mojeid_value=json_decode($this->_db->loadResult());
				if(empty($mojeid_value)) {
					$query=$this->_db->getQuery(true);
					$query->select('COUNT(*)');
					$query->from('#__user_profiles');
					$query->where('user_id='.$mojeid_user_id);
					$query->where('profile_key='.$this->_db->quote($item->key));
					$this->_db->setQuery($query);
					$new=!(bool)$this->_db->loadResult();
					
					$query=$this->_db->getQuery(true);
					if($new) {
						$query->insert('#__user_profiles');
						$query->set('profile_key='.$this->_db->quote($item->key));
						$query->set('user_id='.$mojeid_user_id);
					} else {
						$query->update('#__user_profiles');
						$query->where('profile_key='.$this->_db->quote($item->key));
						$query->where('user_id='.$mojeid_user_id);	
					}
					$query->set('profile_key='.$this->_db->quote($item->key));
					$query->set('profile_value='.$this->_db->quote($item->value));
					$this->_db->setQuery($query);
					if(!$this->_db->query()) {
						$this->setError($this->_db->stderr(true));
						return false;
					}
				}
			}
			
			$query=$this->_db->getQuery(true);
			$query->delete('#__user_profiles');
			$query->where('user_id='.$local_account_user_id);
			$query->where('profile_key LIKE '.$this->_db->quote('spaceshop.%'));
			$this->_db->setQuery($query);
			if(!$this->_db->query()) {
				$this->setError($this->_db->stderr(true));
				return false;
			}	
		}
		
		if(!$local_user->delete()) {
			$this->setError($local_user->getError());
			return false;
		}
		
		if(!$mojeid_user->save()) {
			$this->setError($mojeid_user->getError());
			return false;
		}
		
		return true;
	}
}
?>