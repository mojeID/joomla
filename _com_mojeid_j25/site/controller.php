<?php
/**
* Created on Sep 24, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.comonent.controller');

class MojeIDController extends JControllerLegacy { 
	function display($cache=false,$params=false){
		JRequest::setVar('view',$view=JRequest::getCmd('view','localaccount'));
		JRequest::setVar('layout',$layout=JRequest::getCmd('layout','default'));
		parent::display($cache,$params);
	}
}
?>