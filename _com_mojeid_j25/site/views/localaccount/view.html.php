<?php
/**
* Created on Sep 24, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class MojeIDViewLocalAccount extends JViewLegacy {
	function display($tpl=null) {
		$this->params=JFactory::getApplication()->getParams();
		$this->prepareDocument();
		parent::display($tpl);
	}
	
	protected function prepareDocument() {
		echo JHtml::_('behavior.formvalidation');
	}
}
?>