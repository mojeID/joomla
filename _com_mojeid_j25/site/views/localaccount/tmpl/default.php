<?php
/**
* Created on Sep 24, 2014
* @package   prj_mojeid_j25
* @author    Filip Bartmann, Martin Kušnir
* @copyright Copyright (C) WebSite21 | www.website21.cz | www.spaceshop.cz | www.joomladev.eu |
* @license   http://www.gnu.org/licenses/gpl-3.0.html | http://www.gnugpl.cz/v3/
*/

defined('_JEXEC') or die('Restricted access');


?>
<div class="localaccount_page<?php echo $this->params->get('pageclass_sfx'); ?>">
	<form action="<?php echo JRoute::_('index.php?option=com_mojeid&task=localaccount.bind'); ?>" method="post" name="localaccountForm" id="localaccountForm" class="form-validate">
		<table>
			<tr>
				<th><?php echo JText::_('COM_MOJEID_USERNAME'); ?></td>
				<td>
					<input required="required" type="text" name="username" id="username" value="" />
				</td>
			</tr>
			<tr>
				<th><?php echo JText::_('COM_MOJEID_PASSWORD'); ?></th>
				<td>
					<input required="required" type="password" name="password" id="password" value="" />
				</td>
			</tr>
		</table>
		<button type="submit" class="button btn btn-success validate"><?php echo JText::_('COM_MOJEID_VALIDATE_LOCAL_ACCOUNT'); ?></button>
		<input type="hidden" name="option" value="com_mojeid" />
		<input type="hidden" name="task" value="localaccount.bind" />
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>