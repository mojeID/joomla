<?php
$path_extra=dirname(__FILE__);
$path = ini_get('include_path');
$path = $path_extra . PATH_SEPARATOR . $path;
ini_set('include_path', $path);


function doIncludes() {
    /**
     * Require the OpenID consumer code.
     */
    require_once dirname(__FILE__)."/Auth/OpenID/Consumer.php";

    /**
     * Require the "file store" module, which we'll need to store
     * OpenID information.
     */
    require_once dirname(__FILE__)."/Auth/OpenID/FileStore.php";

    /**
     * Require the Simple Registration extension API.
     */
    require_once dirname(__FILE__)."/Auth/OpenID/SReg.php";

	/**
     * Require the PAPE extension module.
     */
    require_once dirname(__FILE__)."/Auth/OpenID/AX.php";
    
    /**
     * Require the PAPE extension module.
     */
    require_once dirname(__FILE__)."/Auth/OpenID/PAPE.php";
}

doIncludes();

global $pape_policy_uris;
$pape_policy_uris = array(
			  PAPE_AUTH_MULTI_FACTOR_PHYSICAL,
			  PAPE_AUTH_MULTI_FACTOR,
			  PAPE_AUTH_PHISHING_RESISTANT
			  );

function &getStore() {
    /**
     * This is where the example will store its OpenID information.
     * You should change this path if you want the example store to be
     * created elsewhere.  After you're done playing with the example
     * script, you'll have to remove this directory manually.
     */
    
    $store_path=JFactory::getApplication()->getCfg('tmp_path');

    if (!file_exists($store_path) &&
        !mkdir($store_path)) {
        print "Could not create the FileStore directory '$store_path'. ".
            " Please check the effective permissions.";
        exit(0);
    }
	$r = new Auth_OpenID_FileStore($store_path);

    return $r;
}

function &getConsumer() {
    /**
     * Create a consumer object using the store object created
     * earlier.
     */
    $store = getStore();
	$r = new Auth_OpenID_Consumer($store);
    return $r;
}

function getScheme() {
    $scheme = 'http';
    if (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') {
        $scheme .= 's';
    }
    return $scheme;
}

function getReturnTo() {
	return str_replace('http://','https://',JFactory::getURI()->base()).'index.php?mojeid_login=2';
}

function getTrustRoot() {
	return str_replace('http://','https://',JFactory::getURI()->base());
}

?>